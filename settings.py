import os


class Settings:

    OPENAI_API_KEY = os.environ["OPENAI_API_KEY"]
    ACTIVELOOP_TOKEN = os.environ["ACTIVELOOP_TOKEN"]
